# Jessie is the most current OS supported by MongoDB
#  - https://docs.mongodb.com/manual/tutorial/install-mongodb-on-debian/
#  - See https://packages.debian.org/jessie/mongodb-server vs. https://packages.debian.org/stretch/mongodb-server
FROM arm32v7/debian:jessie
MAINTAINER Bryan Davidson <bryan@davidsons.name> 

# http://www.lowefamily.com.au/2016/06/02/installing-ubiquiti-unifi-controller-5-on-raspberry-pi/3/
# https://github.com/jacobalberty/unifi-docker/blob/master/unifi5/Dockerfile
# https://github.com/amitgandhinz/docker-unifi

# https://help.ubnt.com/hc/en-us/articles/218506997-UniFi-Ports-Used
EXPOSE 3478/udp 6789/tcp 8080/tcp 8443/tcp 8843/tcp 8880/tcp

# https://help.ubnt.com/hc/en-us/articles/204952144-UniFi-How-can-I-restore-a-backup-configuration-
VOLUME ["/usr/lib/unifi/data", "/var/lib/unifi"]

# Need Java 8 so mobile applications can connect to the controller
# Oracle Java is required as of 5.4.16
#  - https://community.ubnt.com/t5/UniFi-Wireless/UniFi-5-4-16-Stable-has-been-released/td-p/1931620
#  - http://www.webupd8.org/2014/03/how-to-install-oracle-java-8-in-debian.html
RUN echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" | tee /etc/apt/sources.list.d/webupd8team-java.list
RUN echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" | tee -a /etc/apt/sources.list.d/webupd8team-java.list
RUN apt-get update -q && \
  apt-get install -qy gnupg
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886
RUN echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections

# https://help.ubnt.com/hc/en-us/articles/220066768-UniFi-How-to-Install-Update-via-APT-on-Debian-or-Ubuntu
RUN echo 'deb http://www.ubnt.com/downloads/unifi/debian unifi5 ubiquiti' | tee -a /etc/apt/sources.list.d/100-ubnt.list > /dev/null && \
  apt-key adv --keyserver keyserver.ubuntu.com --recv 06E85760C0A52C50
RUN apt-get update -q && \
  apt-get install -qy ca-certificates-java oracle-java8-installer && \
  apt-get install -qy unifi

RUN echo 'ENABLE_MONGODB=no' | tee -a /etc/mongodb.conf > /dev/null

WORKDIR /var/lib/unifi

CMD ["/usr/bin/java", "-Xmx1024M", "-jar", "/usr/lib/unifi/lib/ace.jar", "start"]
